awk examples
============


Remove 3 char from $3 that is the third field separated by dot and print it
---------------------------------------------------------------------------

```
$ cat test
https://www.example1.com/ciao1/ciaon1
https://www.example2.com/ciao2/ciaon2
https://www.example3.com/ciao3/ciaon3
https://www.example4.com/helloworld
https://www.example5.com/h/e/l/l/o

$ awk -F. '{gsub(/[a-z]{3}\//,"",$3); print $3}' test
ciao1/ciaon1
ciao2/ciaon2
ciao3/ciaon3
helloworld
h/e/l/l/o
```

Create an array with awk (First row add example for printing the array id)
--------------------------------------------------------------------------

```
$ awk '{ n = split($0, t, "|"); print t[2] }' <<<'12|23|11'
23
$ awk '{ n = split($0, t, "|"); print t[1] }' <<<'12|23|11'
12
```

For loop (https://linuxhint.com/for_loop_awk_command/)
------------------------------------------------------

```
$ echo | awk '{ for (counter = 10; counter >= 5; counter--) print "Running for ",counter, " times.", "\n"; }'
Running for  10  times.

Running for  9  times.

Running for  8  times.

Running for  7  times.

Running for  6  times.

Running for  5  times.

```


